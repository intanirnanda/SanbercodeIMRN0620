var readBooksPromise = require('./promise.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

const letStart = (index, times) => {
    index > books.length - 1 ? '' :
        readBooksPromise(times, books[index])
        .then(function (sisaWaktu) {
            return index + letStart (index + 1, sisaWaktu)
        })
        .catch(function (sisaWaktu) {
            ''
        })
}
letStart(0, 8000)
