var readBooks = require('./callback.js')

var books = [{
        name: 'LOTR',
        timeSpent: 3000
    },
    {
        name: 'Fidas',
        timeSpent: 2000
    },
    {
        name: 'Kalkulus',
        timeSpent: 4000
    }
]

const letStart = (index, times) => {
    index > books.length -1 ? '' :
    readBooks(times, books[index], times => {
        return index + letStart (index + 1, times)
    })
}

// var start = function letStart(index, times) {
//     if (index > books.length-1) {
//         readBooks(times, books[index], function times(){
//             return index + letStart(index + 1, times)
//         })
//     }
// }

letStart(0,8000)
