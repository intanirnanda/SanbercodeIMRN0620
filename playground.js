console.log("=============== Soal No 1 ===============");

function arrayToObject(params=[[]]) {
    var now = new Date()
    var thisYear = now.getFullYear()
    for (let index = 0; index < params.length; index++) {
        var age = 0
        if (thisYear - params[index][3]>0) {
            age = thisYear - params[index][3]
        }else age = "Invalid Birth Year"
        var result = {
            firstname: params[index][0],
            lastname: params[index][1],
            gender: params[index][2],
            age: age
        }
        console.log(`${index+1}. ${result.firstname} ${result.lastname}:{
            firstName: "${result.firstname}",
            lastname: "${result.lastname}",
            gender: "${result.gender}",
            age: ${result.age}
        }`);
    }
}
var people = [
    ["Bruce", "Banner", "male", 1975], 
    ["Natasha", "Romanoff", "female"]
]
arrayToObject(people) 
var people2 = [
    ["Tony", "Stark", "male", 1980],
    ["Pepper", "Pots", "female", 2023]
]
arrayToObject(people2)
console.log();

console.log("=============== Soal No 2 ===============");

var Toko= {
    'Sepatu Stacattu' : 1500000,
    'Baju Zoro' : 500000,
    'Baju H&N' : 250000,
    'Sweater Uniklooh' : 175000,
    'Casing Handphone' : 50000
}
function shoppingTime(memberId, money){
    var purchaseInfo = {}
    if (memberId == undefined || memberId == "") {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    }else if (money < 50000) {
        return "Mohon maaf, uang tidak cukup"
    }else{
        purchaseInfo.memberId = memberId
        purchaseInfo.money = money
        var arrListPurchased = []
        if (money>=Toko["Sepatu Stacattu"]) {
            arrListPurchased.push("Sepatu Stacattu")
            money -= Toko["Sepatu Stacattu"]
        }
        if (money>=Toko["Baju Zoro"]) {
            arrListPurchased.push("Baju Zoro")
            money -= Toko["Baju Zoro"]
        }
        if (money>=Toko["Baju H&N"]) {
            arrListPurchased.push("Baju H&N")
            money -= Toko["Baju H&N"]
        }
        if (money >= Toko["Sweater Uniklooh"]) {
            arrListPurchased.push("Sweater Uniklooh")
            money -= Toko["Sweater Uniklooh"]
        }
        if (money>=Toko["Casing Handphone"]) {
            arrListPurchased.push("Casing Handphone")
            money -= Toko["Casing Handphone"]
        }
        purchaseInfo.listPurchased = arrListPurchased
        purchaseInfo.changeMoney = money
    }
    return purchaseInfo
}
console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
console.log();

console.log("=============== Soal No 3 ===============");

function naikAngkot(arrPenumpang=[[]]) {
    var rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrResult = []
    for (let i = 0; i < arrPenumpang.length; i++) {
        var strRute = rute.join("");
        var ruteCounter = Number(strRute.indexOf(arrPenumpang[i][2])) - Number(strRute.indexOf(arrPenumpang[i][1]))
        var bayar = ruteCounter*2000
        var penumpang = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: bayar
        }
        arrResult.push(penumpang)
    }
    return arrResult
}
console.log(naikAngkot([
    ['Dimitri', 'B', 'F'],
    ['Icha', 'A', 'B']
]));
