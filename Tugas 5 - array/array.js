console.log("========= Soal no. 1 Range ==========")
function range(start = null, end = null)
{
 
  var myarray = [];
  if(start && end == null)
  {
     return -1;
  }else if(start < end)
  {
    for(var i = start; i<=end; i++){
      myarray.push(i);
    }
    return myarray;
  }else if(start > end)
  {
    for(var j = start; j>=end; j--){
       myarray.push(j);
    }
    return myarray;
  }else{
     return -1;
  }
}

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

console.log("========= Soal no. 2 Range with step ==========")
function rangeWithStep(startNum, finishNum, step) {
    var result = []
    if (startNum == undefined && finishNum == undefined && step == undefined) return -1
    if (startNum > finishNum) {for (let index = startNum; index >= finishNum; index-= step) result.push(index);}
    else {for (let index = startNum; index <= finishNum; index+= step) result.push(index);}
    return result
}
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) 

console.log("========= Soal no. 3 Sum of Range ==========")
function sum(first, last, step) {
    var result = 0
    //cek parameter
    if (first == undefined) return 0
    else if (last == undefined) return first
    else if (step == undefined) step = 1
    //summary
    if (first > last) {for (let index = first; index >= last; index-=step) {result += index}}
    else {for (let index = first; index <= last; index+=step) {result += index}}
    return result
}
console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 
console.log(" ");

console.log("========= Soal no. 4 Array Multidimensi ==========")
var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]
function dataHandling(data) {
    for (let index = 0; index < data.length; index++) {
        console.log(`Nomor ID: ${data[index][0]}`);
        console.log(`Nama Lengkap: ${data[index][1]}`);
        console.log(`TTL: ${data[index][2]} ${data[index][3]}`);
        console.log(`Hobi: ${data[index][4]}`);
        console.log();
    }
}
dataHandling(input)

console.log("========= Soal no. 5 Balik kata ==========")
function balikKata(str) {
        let reversed = "";
        for (var i = str.length - 1; i >= 0; i--) {
            reversed += str[i];
        }
        return reversed;
}
console.log(balikKata("Kasur Rusak")); // kasuR rusaK
console.log(balikKata("SanberCode")); // edoCrebnaS
console.log(balikKata("Haji Ijah")); // hajI ijaH
console.log(balikKata("racecar")); // racecar
console.log(balikKata("I am Sanbers")); // srebnaS ma I 
console.log(" ");

console.log("========= Soal no. 6 Metode Array ==========")
var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(data = []) {
    var firstName = data[1]
    data.splice(1, 1, `${firstName} Elsharawy`)
    data.splice(2, 1,`Provinsi ${data[2]}`)
    data.splice(4,1,"Pria", "SMA Internasional Metro")
    var birthdate = data[3].split("/")
    console.log(data);
    switch (Number(birthdate[1])) {
        case 01: console.log("Januari"); break;
        case 02: console.log("Februari"); break;
        case 03: console.log("Maret"); break;
        case 04: console.log("April"); break;
        case 05: console.log("Mei"); break;
        case 06: console.log("Juni"); break;
        case 07: console.log("Juli"); break;
        case 08: console.log("Agustus"); break;
        case 09: console.log("September"); break;
        case 10: console.log("Oktober"); break;
        case 11: console.log("November"); break;
        case 12: console.log("Desember"); break;
        default: break;
    }
  
    var reverseDate = [birthdate[2], birthdate[0], birthdate[1]]
    console.log(reverseDate);
    console.log(birthdate.join("-"));
    console.log(firstName);
}
dataHandling2(input);
