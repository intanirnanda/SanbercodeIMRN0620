console.log("LOOPING PERTAMA");

var loop = 2;
while (loop <= 20) {
    console.log(loop + ' - ' + "i love coding");
    loop += 2;
}

console.log("LOOPING KEDUA");
var loop = 20;
while (loop > 0) {
    console.log(loop + ' - ' + "i will become a mobile developer");
    loop -= 2;
}

console.log(" ");

// for(var angka = 1; angka <= 20; angka++){
//     if(angka % 2 == 0){
//         console.log(angka + " - " + "Berkualitas");
//     }else {
//         if(angka % 3 == 0){
//             if(angka % 2 == 1){
//                 console.log(angka + " - " + "I Love Coding");
//             }
//         }else {
//             console.log(angka + " - " + "Santai");
//         }
//     }
// }

for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 == 0) {
        console.log(angka + " - " + "Berkualitas");
    } else if (angka % 3 == 0 && angka % 2 == 1) {
        console.log(angka + " - " + "I Love Coding");
    } else {
        console.log(angka + " - " + "Santai");
    }
}

console.log(" ");

var baris = "";
for (var i = 0; i < 4; i++) {
  for (var a = 0; a < 9; a++) {
    baris += (a % 9) == (i % 4) ? "" : "#";
  }
  baris += "\n";
}
console.log(baris);

for (var i = 0; i < 7; i++) {
  console.log("#".repeat(i + 1))
}

console.log(" ");

var catur = "";
for (var i = 0; i < 8; i++) {
  for (var a = 0; a < 8; a++) {
    catur += (a % 2) == (i % 2) ? " " : "#";
  }
  catur += "\n";
}
console.log(catur); 