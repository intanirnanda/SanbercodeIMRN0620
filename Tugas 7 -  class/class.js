//Soal no.1 (Animal Class)
//Release 0
class Animal {
    constructor(name){
        this._name = name
        this._legs = 4
        this._cold_blooded = false
    }
}
var sheep = new Animal("shaun");
console.log(sheep._name) // "shaun"
console.log(sheep._legs) // 4
console.log(sheep._cold_blooded) // false

//Release 1
class Ape extends Animal {
    constructor(name){
        super(name)
        this._legs = 2
    }
    yell(){console.log("Auooo")}
}
class Frog extends Animal {
    constructor(name){super(name)}
    jump(){console.log("hop hop")}
}
var sungokong = new Ape("kera sakti")
sungokong.yell() // "Auooo"
var kodok = new Frog("buduk")
kodok.jump() // "hop hop"

//soal no.2 (Function to class)
// function Clock({ template }) {

//     var timer;

//     function render() {
//         var date = new Date();

//         var hours = date.getHours();
//         if (hours < 10) hours = '0' + hours;

//         var mins = date.getMinutes();
//         if (mins < 10) mins = '0' + mins;

//         var secs = date.getSeconds();
//         if (secs < 10) secs = '0' + secs;

//         var output = template
//             .replace('h', hours)
//             .replace('m', mins)
//             .replace('s', secs);

//         console.log(output);
//     }

//     this.stop = function () {
//         clearInterval(timer);
//     };

//     this.start = function () {
//         render();
//         timer = setInterval(render, 1000);
//     };

// }
class Clock{
    constructor({template}){
        this._template = template
        this._timer;
        this._output;
    }
    get template(){
        return this._template
    }
    set template(x){
        this._template = x
    }
    get output(){
        return this._output
    }
    set output(x){
        this._output = x
    }
    get timer(){
        return this._timer
    }
    set timer(x){
        this._timer = x
    }
    
    render() {
        var date = new Date();

        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;

        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;

        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;

        this.output = this._template
            .replace('h', hours)
            .replace('m', mins)
            .replace('s', secs);
        console.log(this.output);
    }
    stop(){
        clearInterval(timer);
    }
    start(){
        this.render();
        // this.timer = setInterval(this.render.bind(this), 1000);
        this.timer = setInterval(() => {this.render()}, 1000);
    }
}

var clock = new Clock({template:'h:m:s'});
clock.start(); 
